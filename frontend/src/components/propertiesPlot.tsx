import React, { useState, useEffect } from 'react';
import { Container, Col, Row, Card, Form, Alert } from 'react-bootstrap';
import axios from 'axios';
import { DFTCALCULATIONS, IDFTCALCULATIONS } from '../api/Endpoints';
import SignalPlot from '../components/FourierPlot';

const PropertyPlot = ({ property, selectedFunction, isSymmetrySection = false, isPeriodicitySection = false }) => {
  const [plotData1, setPlotData1] = useState<number[]>([]);
  const [plotData2, setPlotData2] = useState<number[]>([]);
  const [combinedPlot, setCombinedPlot] = useState<number[]>([]);
  const [dftAbs1, setDftAbs1] = useState<number[]>([]);
  const [dftAbs2, setDftAbs2] = useState<number[]>([]);
  const [resultPlot, setResultPlot] = useState<number[]>([]);
  const [idftPlot, setIdftPlot] = useState<number[]>([]);
  const [propertyDescription, setPropertyDescription] = useState<string>('');
  const [error, setError] = useState<string | null>(null);

  const [amplitude1, setAmplitude1] = useState(1);
  const [frequency1, setFrequency1] = useState(1);
  const [amplitude2, setAmplitude2] = useState(1);
  const [frequency2, setFrequency2] = useState(1);
  const [phaseShift, setPhaseShift] = useState(0);

  const N = 500;
  const periods = 1;

  const xValues = Array.from({ length: N * periods }, (_, x) => x);

  const generateSignal = (x, amplitude, frequency) => {
    const adjustedX = (x * frequency + phaseShift) % N;
    switch (selectedFunction) {
      case 'sin':
        return amplitude * Math.sin(2 * Math.PI * adjustedX / N);
      case 'cos':
        return amplitude * Math.cos(2 * Math.PI * adjustedX / N);
      case 'square':
        return amplitude * Math.sign(Math.sin(2 * Math.PI * adjustedX / N));
      case 'triangle':
        return amplitude * 2 * Math.abs((adjustedX / N) - Math.floor((adjustedX / N) + 0.5));
      default:
        return amplitude * Math.sin(2 * Math.PI * adjustedX / N);
    }
  };

  const handleDFTCalculations = async (signal1, signal2, combined) => {
    const formatData = (signal) => ({
      coefficients: signal.map(value => ({ real: value.toString(), imaginary: "0" }))
    });
    try {
      const response1 = await axios.post(DFTCALCULATIONS(), formatData(signal1));
      const response2 = await axios.post(DFTCALCULATIONS(), formatData(signal2));
      const responseCombined = await axios.post(DFTCALCULATIONS(), combined);
      const responseIDFT = await axios.post(IDFTCALCULATIONS(), combined);

      const { Magnitude: dftAbs1} = response1.data;
      const { Magnitude: dftAbs2 } = response2.data;
      const { Magnitude: dftRealCombined } = responseCombined.data;

      setDftAbs1(dftAbs1 || []);
      setDftAbs2(dftAbs2 || []);
      setResultPlot(dftRealCombined || []);
      setIdftPlot(responseIDFT.data.Real || []);
      setError(null);
    } catch (error) {
      setError('Error calculating DFT for properties');
      console.error('Error calculating DFT for properties:', error);
    }
  };

  const updatePlots = () => {
    const signal1 = xValues.map((x) => generateSignal(x, amplitude1, frequency1));
    const signal2 = xValues.map((x) => generateSignal(x, amplitude2, frequency2));
    const combined = signal1.map((value, index) => value + signal2[index]);

    setPlotData1(signal1);
    setPlotData2(signal2);
    setCombinedPlot(combined);

    handleDFTCalculations(signal1, signal2, combined);
  };

  useEffect(() => {
    updatePlots();
  }, [amplitude1, frequency1, amplitude2, frequency2, phaseShift, selectedFunction]);

  useEffect(() => {
    switch (property) {
      case 'linearity':
        setPropertyDescription('Linearity: The DFT of a sum of signals is the sum of their DFTs.');
        break;
      case 'symmetry':
        setPropertyDescription('Symmetry: The DFT of a real-valued signal exhibits symmetric properties in its frequency domain representation.');
        break;
      case 'periodicity':
        setPropertyDescription('Periodicity: The DFT is periodic with a period equal to the length of the input signal.');
        break;
      case 'time-shifting':
        setPropertyDescription('Time Shifting: A shift in the time domain corresponds to a phase shift in the frequency domain.');
        break;
      case 'frequency-shifting':
        setPropertyDescription('Frequency Shifting: Multiplying a signal by a complex exponential corresponds to a shift in the frequency domain.');
        break;
      case 'convolution':
        setPropertyDescription('Convolution: The convolution of two signals in the time domain corresponds to the pointwise multiplication of their DFTs in the frequency domain.');
        break;
      case 'IDFT':
        setPropertyDescription('IDFT: Inverse Fourier Transform Function.');
        break;
      default:
        setPropertyDescription('');
    }
  }, [property]);

  const renderLinearityControls = () => (
    <>
      <Row className="mb-3">
        <Col md={6}>
          <Form.Group controlId="formAmplitude1">
            <Form.Label>Amplitude 1: {amplitude1}</Form.Label>
            <Form.Control
              type="range"
              min="10"
              max="200"
              step="10"
              value={amplitude1}
              onChange={(e) => setAmplitude1(Number(e.target.value))}
            />
          </Form.Group>
        </Col>
        <Col md={6}>
          <Form.Group controlId="formFrequency1">
            <Form.Label>Frequency 1: {frequency1}</Form.Label>
            <Form.Control
              type="range"
              min="1"
              max="200"
              step="0.5"
              value={frequency1}
              onChange={(e) => setFrequency1(Number(e.target.value))}
            />
          </Form.Group>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col md={6}>
          <Form.Group controlId="formAmplitude2">
            <Form.Label>Amplitude 2: {amplitude2}</Form.Label>
            <Form.Control
              type="range"
              min="10"
              max="200"
              step="10"
              value={amplitude2}
              onChange={(e) => setAmplitude2(Number(e.target.value))}
            />
          </Form.Group>
        </Col>
        <Col md={6}>
          <Form.Group controlId="formFrequency2">
            <Form.Label>Frequency 2: {frequency2}</Form.Label>
            <Form.Control
              type="range"
              min="1"
              max="20"
              step="0.2"
              value={frequency2}
              onChange={(e) => setFrequency2(Number(e.target.value))}
            />
          </Form.Group>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col md={6}>
          {plotData1.length > 0 && <SignalPlot data={plotData1} title={`Signal 1 (${selectedFunction})`} type="1D" />}
        </Col>
        <Col md={6}>
          {dftAbs1.length > 0 && <SignalPlot data={dftAbs1} title="DFT Magnitude (Signal 1)" type="1D" />}
        </Col>
      </Row>
      <Row className="mb-3">
      <Col md={6}>
          {plotData2.length > 0 && <SignalPlot data={plotData2} title={`Signal 2 (${selectedFunction})`} type="1D" />}
        </Col>
        <Col md={6}>
          {dftAbs2.length > 0 && <SignalPlot data={dftAbs2} title="DFT Magnitude (Signal 2)" type="1D" />}
        </Col>
      </Row>
      <Row className="mb-3">
      <Col md={6}>
          {combinedPlot.length > 0 && <SignalPlot data={combinedPlot} title="Combined Signal" type="1D" />}
        </Col>
        <Col md={6}>
          {resultPlot.length > 0 && <SignalPlot data={resultPlot} title="DFT Result (Combined Signal)" type="1D" />}
        </Col>
      </Row>
      <Row className="mb-3">

        <Col md={6}>
          {idftPlot.length > 0 && <SignalPlot data={idftPlot} title="IDFT of Combined Signal" type="1D" />}
        </Col>
      </Row>
    </>
  );

  const renderPropertySpecificContent = () => {
    switch (property) {
      case 'linearity':
        return renderLinearityControls();
      case 'symmetry':
      case 'periodicity':
      case 'time-shifting':
      case 'frequency-shifting':
      case 'convolution':
      case 'IDFT':
        return renderLinearityControls();
      default:
        return null;
    }
  };

  return (
    <Card className="mt-3">
      <Card.Body>
        <Card.Title>{property}</Card.Title>
        <Card.Text>{propertyDescription}</Card.Text>
        {renderPropertySpecificContent()}
        {error && <Alert variant="danger">{error}</Alert>}
      </Card.Body>
    </Card>
  );
};

export default PropertyPlot;
