import React from 'react';
import Plot from 'react-plotly.js';

function SignalPlot({ data, title, type, isSymmetrySection = false, isPeriodicitySection = false, periods = 2 }) {
  const plotData = [];

  if (!data) {
    return <div>No data provided</div>;
  }

  if (type === '1D') {
    plotData.push({
      type: 'scatter',
      mode: 'markers',
      x: Array.from({ length: data.length }, (_, i) => i),
      y: data,
      marker: {
        color: 'black', // Change marker color to black
      },
    });
  } else if (type === '2D') {
    plotData.push({
      type: 'heatmap',
      z: data,
      colorscale: [[0, 'black'], [1, 'white']], // Black and white color scale
    });
  }

  return (
    <Plot
      data={plotData}
      layout={{
        width: 600,
        height: 500,
        title,
        xaxis: {
          title: type === '1D' ? '' : 'X-axis',
        },
        yaxis: {
          title: type === '1D' ? { title } : 'Y-axis',
        },
      }}
    />
  );
}

export default SignalPlot;
