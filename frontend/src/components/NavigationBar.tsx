import React, { useState } from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import './styles.css';

function NavigationBar() {
  const history = useHistory();

  const [fourierOpen, setFourierOpen] = useState(false);
  const [cosineOpen, setCosineOpen] = useState(false);

  const handleTransformSelection = (transformType: string, option: string) => {
    const path = `/${transformType}/${option}`;
    history.push(path);
  };

  const handleMouseEnter = (type: string) => {
    if (type === 'fourier') setFourierOpen(true);
    else if (type === 'cosine') setCosineOpen(true);
  };

  const handleMouseLeave = (type: string) => {
    if (type === 'fourier') setFourierOpen(false);
    else if (type === 'cosine') setCosineOpen(false);
  };

  return (
    <Navbar className="justify-content-center" expand="lg">
      <Navbar.Brand id="navbar-brand" href="/">Discrete Transforms</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mx-auto">
          <NavDropdown
            title="Fourier Transform"
            id="fourier-dropdown"
            show={fourierOpen}
            onMouseEnter={() => handleMouseEnter('fourier')}
            onMouseLeave={() => handleMouseLeave('fourier')}
            className="nav-item"
          >
            <NavDropdown.Item onClick={() => handleTransformSelection('fourier', 'calculator')}>Calculator</NavDropdown.Item>
            <NavDropdown.Item onClick={() => handleTransformSelection('fourier', 'learning')}>Learning</NavDropdown.Item>
          </NavDropdown>
          <NavDropdown
            title="Cosine Transform"
            id="cosine-dropdown"
            show={cosineOpen}
            onMouseEnter={() => handleMouseEnter('cosine')}
            onMouseLeave={() => handleMouseLeave('cosine')}
            className="nav-item"
          >
            <NavDropdown.Item onClick={() => handleTransformSelection('cosine', 'calculator')}>Calculator</NavDropdown.Item>
            <NavDropdown.Item onClick={() => handleTransformSelection('cosine', 'learning')}>Learning</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavigationBar;
