import React from 'react';
import { Nav } from 'react-bootstrap';
import { Link } from 'react-scroll';

const NavigationMenu = () => {
    const sections = ['Introduction', 'Sampling', 'DFT Calculation', 'DFT Magnitude', 'DFT Phase'];
    return (
      <div className="sticky-nav">
        <Nav className="flex-column">
          {sections.map((section, index) => (
            <Nav.Link key={index} as={Link} to={section.toLowerCase().replace(/\s/g, '-')} smooth={true} duration={500}>{section}</Nav.Link>
          ))}
        </Nav>
      </div>
    );
  };
export default NavigationMenu;
