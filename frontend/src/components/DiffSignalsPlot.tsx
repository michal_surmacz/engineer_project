import React, { useState } from 'react';
import { Container, Col, Row, Button, Form } from 'react-bootstrap';
import axios from 'axios';
import SignalPlot from '../components/FourierPlot';
import { DFTCALCULATIONS } from '../api/Endpoints';

const SignalsPlot = ({ selectedFunction, setSelectedFunction }) => {
  const [plotData, setPlotData] = useState([]);
  const [dftMagnitude, setDftMagnitude] = useState([]);
  const [dftPhase, setDftPhase] = useState([]);
  const [samplingRate, setSamplingRate] = useState(15);
  const [showDFTButton, setShowDFTButton] = useState(false);

  const handleSampling = () => {
    const xValues = [];
    for (let x = 0; x <= 100; x += 1 / samplingRate) {
      xValues.push(x);
    }
    let signalArray;
    switch (selectedFunction) {
      case 'sin':
        signalArray = xValues.map((x) => Math.sin(x));
        break;
      case 'cos':
        signalArray = xValues.map((x) => Math.cos(x));
        break;
      case 'square':
        signalArray = xValues.map((x) => Math.sign(Math.sin(x)));
        break;
      case 'triangle':
        signalArray = xValues.map((x) => Math.abs(2 * (x / Math.PI - Math.floor(x / Math.PI + 0.5))) * 2 - 1);
        break;
      case 'impulse':
        signalArray = xValues.map((x) => (x === 10 ? 1 : 0));
        break;
      case 'gaussian':
        signalArray = xValues.map((x) => Math.exp(-Math.pow(x - 10, 2) / 2));
        break;
      case 'random':
        signalArray = xValues.map(() => Math.random() * 2 - 1);
        break;
      default:
        signalArray = xValues.map((x) => Math.sin(x));
    }
    setPlotData(signalArray);
    setShowDFTButton(true);
  };

  const handleDFT = async () => {
    const formatData = (signal) => ({
      coefficients: signal.map((value) => ({ real: value.toString(), imaginary: '0' })),
    });

    try {
      const response = await axios.post(DFTCALCULATIONS(), formatData(plotData));
      const { Magnitude } = response.data;

      const normalizedMagnitude = Magnitude.map((value) => Math.abs(value));

      setDftMagnitude(normalizedMagnitude);
    } catch (error) {
      console.error('Error calculating DFT:', error);
    }
  };

  return (
    <Container>
      <Row>
        <Col>
          <Form.Group controlId="formFunctionSelect">
            <Form.Label>Select Function</Form.Label>
            <Form.Control as="select" value={selectedFunction} onChange={(e) => setSelectedFunction(e.target.value)}>
              <option value="sin">Sine</option>
              <option value="cos">Cosine</option>
              <option value="square">Square Wave</option>
              <option value="triangle">Triangle Wave</option>
              <option value="impulse">Impulse Signal</option>
              <option value="gaussian">Gaussian Function</option>
              <option value="random">Random Signal</option>
            </Form.Control>
          </Form.Group>
          <Button variant="primary" onClick={handleSampling}>
            Sample {selectedFunction}(x)
          </Button>
        </Col>
      </Row>
      <Row>
        <Col>
          {plotData.length > 0 && <SignalPlot data={plotData} title={`${selectedFunction}(x) Plot`} type="1D" />}
        </Col>
      </Row>
      {showDFTButton && (
        <Row>
          <Col>
            <Button variant="success" onClick={handleDFT} disabled={plotData.length === 0}>
              Calculate DFT
            </Button>
          </Col>
        </Row>
      )}
      <Row>
        <Col>
          {dftMagnitude.length > 0  && (
            <SignalPlot data={dftMagnitude} title="Magnitude" type="1D" />
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default SignalsPlot;
