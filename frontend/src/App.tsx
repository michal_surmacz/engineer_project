import * as React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import WelcomePage from "./pages/WelcomePage";
import FourierCalculatorPage from "./pages/FourierCalculatorPage";
import ForuierLearningPage from "./pages/FourierLearningPage";
import CosineCalculatorPage from "./pages/CosineCalculatorPage";
import CosineLearningPage from "./pages/CosineLearningPage";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" render={() => <WelcomePage />} />
        <Route path="/fourier/calculator" render={() => <FourierCalculatorPage />} />
        <Route path="/fourier/learning" render={() => <ForuierLearningPage />} />
        <Route path="/cosine/calculator" render={() => <CosineCalculatorPage />} />
        <Route path="/cosine/learning" render={() => <CosineLearningPage />} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;