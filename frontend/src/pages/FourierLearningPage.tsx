import React, { useState } from 'react';
import { Container, Col, Row, Card, Nav, Button } from 'react-bootstrap';
import { Link } from 'react-scroll';
import NavigationBar from '../components/NavigationBar';
import PropertyPlot from '../components/propertiesPlot';
import SignalsPlot from "../components/DiffSignalsPlot";
import './styles.css';

function FourierLearningPage() {
  const [selectedFunction, setSelectedFunction] = useState('sin');
  const [showIDFTPlot, setShowIDFTPlot] = useState(false);
  const [visibleProperties, setVisibleProperties] = useState<Record<string, boolean>>({});

  const handleFunctionChange = (e: any) => {
    setSelectedFunction(e.target.value);
  };

  const handleShowPropertyPlot = (property: string) => {
    setVisibleProperties(prevState => ({
      ...prevState,
      [property]: !prevState[property]
    }));
  };

  return (
    <div>
      <NavigationBar />
      <Container fluid>
        <Row>
          <Col sm={3}>
            <NavigationMenu />
          </Col>
          <Col sm={9}>
            <Card id="introduction" className="transform-card">
              <Card.Body>
                <Card.Title>Introduction</Card.Title>
                <Card.Text>
                  The Discrete Fourier Transform (DFT) is one of the most popular and efficient procedures found in digital signal processing. DFT allows us to analyze, investigate, and synthesize signals in a way that is not possible in continuous (analog) signal processing. Understanding DFT is essential for anyone working in digital signal processing. It determines the harmonic content or frequency of a discrete signal, which is useful for analyzing arbitrary discrete sequences.
                </Card.Text>
              </Card.Body>
            </Card>
            <Card id="dft-calculation" className="transform-card">
              <Card.Body>
                <Card.Title>DFT Calculation</Card.Title>
                <Card.Text>
                  The Discrete Fourier Transform (DFT) transforms a sequence of complex numbers representing a discrete signal from its time domain representation into its frequency domain representation. Sampling a well-known function like a sinusoid helps us understand the capabilities of DFT. Click the button below to sample the selected function.
                </Card.Text>
                <SignalsPlot selectedFunction={selectedFunction} setSelectedFunction={setSelectedFunction} />
              </Card.Body>
            </Card>
            <Card id="linearity" className="transform-card">
              <Card.Body>
                <Card.Title>Linearity of DFT</Card.Title>
                <Card.Text>
                  The DFT is a linear transformation, meaning that the DFT of a sum of signals is the sum of their individual DFTs. This property allows us to analyze complex signals by breaking them down into simpler components.
                </Card.Text>
                <Button onClick={() => handleShowPropertyPlot('linearity')}>Show Linearity</Button>
                {visibleProperties['linearity'] && <PropertyPlot property="linearity" selectedFunction={selectedFunction} />}
              </Card.Body>
            </Card>
            <Card id="symmetry" className="transform-card">
              <Card.Body>
                <Card.Title>Symmetry of DFT</Card.Title>
                <Card.Text>
                  For real-valued signals, the DFT exhibits symmetry properties where the real part of the DFT is an even function, and the imaginary part is an odd function. This property simplifies the analysis of real-valued signals in the frequency domain.
                </Card.Text>
                <Button onClick={() => handleShowPropertyPlot('symmetry')}>Show Symmetry</Button>
                {visibleProperties['symmetry'] && <PropertyPlot property="symmetry" selectedFunction={selectedFunction} isSymmetrySection={true} />}
              </Card.Body>
            </Card>
            <Card id="periodicity" className="transform-card">
              <Card.Body>
                <Card.Title>Periodicity of DFT</Card.Title>
                <Card.Text>
                  The DFT is periodic with a period equal to the length of the input signal. This means that the frequency domain representation of a signal repeats itself every N points, where N is the number of samples in the input signal.
                </Card.Text>
                <Button onClick={() => handleShowPropertyPlot('periodicity')}>Show Periodicity</Button>
                {visibleProperties['periodicity'] && <PropertyPlot property="periodicity" selectedFunction={selectedFunction} isPeriodicitySection={true} />}
              </Card.Body>
            </Card>
            <Card id="time-shifting" className="transform-card">
              <Card.Body>
                <Card.Title>Time Shifting Property</Card.Title>
                <Card.Text>
                  When a signal is shifted in the time domain, its DFT is multiplied by a complex exponential in the frequency domain. This property is useful for analyzing how delays affect the frequency components of a signal.
                </Card.Text>
                <Button onClick={() => handleShowPropertyPlot('time-shifting')}>Show Time Shifting</Button>
                {visibleProperties['time-shifting'] && <PropertyPlot property="time-shifting" selectedFunction={selectedFunction} />}
              </Card.Body>
            </Card>
            <Card id="frequency-shifting" className="transform-card">
              <Card.Body>
                <Card.Title>Frequency Shifting Property</Card.Title>
                <Card.Text>
                  Multiplying a signal by a complex exponential in the time domain results in a shift in the frequency domain. This property allows us to move the frequency components of a signal to different locations in the frequency spectrum.
                </Card.Text>
                <Button onClick={() => handleShowPropertyPlot('frequency-shifting')}>Show Frequency Shifting</Button>
                {visibleProperties['frequency-shifting'] && <PropertyPlot property="frequency-shifting" selectedFunction={selectedFunction} />}
              </Card.Body>
            </Card>
            <Card id="convolution" className="transform-card">
              <Card.Body>
                <Card.Title>Convolution Property</Card.Title>
                <Card.Text>
                  The convolution of two signals in the time domain corresponds to the pointwise multiplication of their DFTs in the frequency domain. This property simplifies the analysis and processing of signals in the frequency domain.
                </Card.Text>
                <Button onClick={() => handleShowPropertyPlot('convolution')}>Show Convolution</Button>
                {visibleProperties['convolution'] && <PropertyPlot property="convolution" selectedFunction={selectedFunction} />}
              </Card.Body>
            </Card>
            <Card id="inverse-dft" className="transform-card">
              <Card.Body>
                <Card.Title>Inverse DFT</Card.Title>
                <Card.Text>
                  The Inverse Discrete Fourier Transform (IDFT) is a mathematical process used to convert a frequency domain representation of a discrete signal back to its time domain representation. It essentially reverses the process of the DFT.
                </Card.Text>
                <Card.Text>
                  <strong>Mathematical Definition:</strong><br />
                  The IDFT of a sequence X[k] of length N is given by:
                  <br />
                  <code>X[n] = (1/N) Σ (from k=0 to N-1) X[k] * e^(j*2πkn/N)</code>
                  <br />
                  where:
                  <ul>
                    <li>X[k] are the DFT coefficients.</li>
                    <li>N is the number of samples.</li>
                    <li>j is the imaginary unit.</li>
                    <li>e^(j*2πkn/N) is the complex exponential function.</li>
                  </ul>
                </Card.Text>
                <Card.Text>
                  <strong>Understanding the IDFT:</strong><br />
                  Just as the DFT decomposes a signal into its constituent frequencies, the IDFT reconstructs the original signal from these frequencies. Each DFT coefficient represents the amplitude and phase of a sinusoidal component of the signal. When these components are summed together in the time domain, they recreate the original signal.
                </Card.Text>
                <Card.Text>
                  <strong>Applications of IDFT:</strong><br />
                  The IDFT is used in many applications, including:
                  <ul>
                    <li><strong>Signal Reconstruction:</strong> Reconstructing signals from their frequency domain representations in various communication systems.</li>
                    <li><strong>Image Processing:</strong> Converting frequency domain processed images back to their spatial domain for display or further analysis.</li>
                    <li><strong>Audio Processing:</strong> Reconstructing audio signals after equalization or other frequency domain modifications.</li>
                  </ul>
                </Card.Text>
                <Button onClick={() => setShowIDFTPlot(!showIDFTPlot)}>Calculate IDFT</Button>
                {showIDFTPlot && <PropertyPlot property="IDFT" selectedFunction={selectedFunction} />}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

const NavigationMenu = () => {
  const sections = [
    { id: 'introduction', title: 'Introduction' },
    { id: 'dft-calculation', title: 'DFT Calculation' },
    { id: 'linearity', title: 'Linearity of DFT' },
    { id: 'symmetry', title: 'Symmetry of DFT' },
    { id: 'periodicity', title: 'Periodicity of DFT' },
    { id: 'time-shifting', title: 'Time Shifting Property' },
    { id: 'frequency-shifting', title: 'Frequency Shifting Property' },
    { id: 'convolution', title: 'Convolution Property' },
    { id: 'inverse-dft', title: 'Inverse DFT' }
  ];

  return (
    <Card className="sticky-top">
      <Card.Body>
        <Card.Title>Fourier Transform</Card.Title>
        <Nav className="flex-column">
          {sections.map((section, index) => (
            <Nav.Link key={index} as={Link} to={section.id.toLowerCase().replace(/\s/g, '-')} smooth={true} duration={500}>{section.title}</Nav.Link>
          ))}
        </Nav>
      </Card.Body>
    </Card>
  );
};

export default FourierLearningPage;
