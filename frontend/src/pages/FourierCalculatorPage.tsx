import React, { useState } from 'react';
import { Container, Button, Form, FormControl, Col, Row, Card, Nav, Tab, FormLabel } from 'react-bootstrap';
import './styles.css';
import SignalPlot from '../components/FourierPlot';
import NavigationBar from '../components/NavigationBar';
import { fetchData } from '../common/calcHandler';

const CalculatorPage = () => {
  const [coefficients, setCoefficients] = useState([{ real: '', imaginary: '' }]);
  const [result, setResult] = useState(null);
  const [activeTab, setActiveTab] = useState('dft');
  const [imageFile, setImageFile] = useState(null);
  const [magnitudeData, setMagnitudeData] = useState(null);

  const handleCoefficientChange = (index: number, type: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    const newCoefficients = [...coefficients];
    newCoefficients[index][type] = event.target.value;
    setCoefficients(newCoefficients);
  };

  const handleAddCoefficient = () => {
    setCoefficients([...coefficients, { real: '', imaginary: '' }]);
  };

  const handleRemoveCoefficient = (index: number) => () => {
    const newCoefficients = [...coefficients];
    newCoefficients.splice(index, 1);
    setCoefficients(newCoefficients);
  };

  const handleImageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setImageFile(event.target.files[0]);
  };

  const handleSubmitDft = async (event: React.FormEvent) => {
    event.preventDefault();
    try {
      const data = await fetchData(
        'http://localhost:5000/api/dft/calculation',
        'POST',
        {
          'Content-Type': 'application/json',
        },
        JSON.stringify({ coefficients })
      );
      setResult(data);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleSubmitDft2d = async (event: React.FormEvent) => {
    event.preventDefault();
    if (!imageFile) {
      console.error('No image file selected');
      return;
    }
    const formData = new FormData();
    formData.append('image', imageFile);

    try {
      const response = await fetch('http://localhost:5000/api/dft2d/calculation', {
        method: 'POST',
        body: formData,
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      const normalizedMagnitude = data.Magnitude.map(row => row.map(val => Math.log1p(val) * 255 / Math.log1p(255) ** 2));
      setMagnitudeData(normalizedMagnitude);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="calculator-page">
      <NavigationBar />
      <Container fluid className="mt-3">
        <Row>
          <Col md={6}>
            <Tab.Container activeKey={activeTab} onSelect={(key: any) => setActiveTab(key)}>
              <Nav variant="tabs">
                <Nav.Item>
                  <Nav.Link eventKey="dft">DFT</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="dft2d">DFT2D</Nav.Link>
                </Nav.Item>
              </Nav>
              <Tab.Content>
                <Tab.Pane eventKey="dft">
                  <Card>
                    <Card.Body>
                      <div className="form-container">
                        <Card.Title>DFT Calculation</Card.Title>
                        <Form onSubmit={handleSubmitDft}>
                          {coefficients.map((coefficient, index) => (
                            <Row key={index} className="mb-3 align-items-center">
                              <Col xs={5}>
                                <FormLabel>Real</FormLabel>
                                <FormControl
                                  type="text"
                                  value={coefficient.real}
                                  onChange={handleCoefficientChange(index, 'real')}
                                />
                              </Col>
                              <Col xs={5}>
                                <FormLabel>Imaginary</FormLabel>
                                <FormControl
                                  type="text"
                                  value={coefficient.imaginary}
                                  onChange={handleCoefficientChange(index, 'imaginary')}
                                />
                              </Col>
                              <Col xs={2}>
                                {index !== 0 && (
                                  <Button variant="danger" onClick={handleRemoveCoefficient(index)}>Remove</Button>
                                )}
                              </Col>
                            </Row>
                          ))}
                          <Button variant="primary" onClick={handleAddCoefficient}>Add Coefficient</Button>
                          <Button variant="success" type="submit" className="ml-2">Calculate DFT</Button>
                        </Form>
                      </div>
                    </Card.Body>
                  </Card>
                </Tab.Pane>
                <Tab.Pane eventKey="dft2d">
                  <Card>
                    <Card.Body>
                      <div className="form-container">
                        <Card.Title>DFT2D Calculation</Card.Title>
                        <Form onSubmit={handleSubmitDft2d}>
                          <Form.Group controlId="formFile" className="mb-3">
                            <FormLabel>Upload Image</FormLabel>
                            <FormControl type="file" accept="image/*" onChange={handleImageChange} />
                          </Form.Group>
                          <Button variant="success" type="submit">Calculate DFT2D</Button>
                        </Form>
                      </div>
                    </Card.Body>
                  </Card>
                </Tab.Pane>
              </Tab.Content>
            </Tab.Container>
          </Col>
          <Col md={6}>
            {magnitudeData && (
              <Card>
                <Card.Body>
                  <SignalPlot data={magnitudeData} title="Magnitude" type="2D" />
                </Card.Body>
              </Card>
            )}
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default CalculatorPage;
