import React, { useState } from 'react';
import { Container, Col, Row, Button, Form, FormControl, Card, Nav, Tab, FormLabel } from 'react-bootstrap';
import './styles.css';
import SignalPlot from '../components/FourierPlot';
import NavigationBar from '../components/NavigationBar';
import { fetchData } from '../common/calcHandler'; // Import fetchData function

const CosineTransformPage = () => {
  const [coefficients, setCoefficients] = useState([{ real: '', imaginary: '' }]);
  const [result, setResult] = useState(null);
  const [activeTab, setActiveTab] = useState('cosineTransform');

  const handleCoefficientChange = (index: number, type: any) => (event: any) => {
    const newCoefficients = [...coefficients];
    newCoefficients[index][type] = event.target.value;
    setCoefficients(newCoefficients);
  };

  const handleAddCoefficient = () => {
    setCoefficients([...coefficients, { real: '', imaginary: '' }]);
  };

  const handleRemoveCoefficient = (index: number) => () => {
    const newCoefficients = [...coefficients];
    newCoefficients.splice(index, 1);
    setCoefficients(newCoefficients);
  };

  const handleSubmitCosineTransform = async (event: any) => {
    event.preventDefault();

    try {
      const data = await fetchData(
        'http:///localhost:5000/api/dct/calculation',
        'POST',
        {
          'Content-Type': 'application/json',
        },
        JSON.stringify({ coefficients })
      );

      setResult(data);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleSubmitCosineTransform2D = async (event: any) => {
    event.preventDefault();

    try {
      const data = await fetchData(
        'http:///localhost:5000/api/dct2d/calculation',
        'POST',
        {
          'Content-Type': 'application/json',
        },
        JSON.stringify({ coefficients })
      );

      setResult(data);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="cosine-transform-page">
      <NavigationBar />
      <Container fluid className="mt-3">
        <Row>
          <Col>
            <Tab.Container activeKey={activeTab} onSelect={(key: any) => setActiveTab(key)}>
              <Nav variant="tabs">
                <Nav.Item>
                  <Nav.Link eventKey="cosineTransform">1D Cosine Transform</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="cosineTransform2D">2D Cosine Transform</Nav.Link>
                </Nav.Item>
              </Nav>
              <Tab.Content>
                <Tab.Pane eventKey="cosineTransform">
                  <Card>
                    <Card.Body>
                      <div className="form-container">
                        <Card.Title>1D Cosine Transform Calculation</Card.Title>
                        <Form onSubmit={handleSubmitCosineTransform}>
                          <Row className="mb-3">
                            <Col xs={5}><FormLabel>Real</FormLabel></Col>
                          </Row>
                          {coefficients.map((coefficient, index) => (
                            <Row key={index} className="mb-3">
                              <Col xs={5}>
                                <FormControl
                                  type="text"
                                  value={coefficient.real}
                                  onChange={handleCoefficientChange(index, 'real')}
                                />
                              </Col>
                              <Col>
                                {index !== 0 && (
                                  <Button variant="danger" className="ml-1" onClick={handleRemoveCoefficient(index)}>Remove</Button>
                                )}
                              </Col>
                            </Row>
                          ))}
                          <Button variant="primary" onClick={handleAddCoefficient}>Add Coefficient</Button>
                          <Button variant="success" type="submit" className="ml-2">Calculate Cosine Transform</Button>
                        </Form>
                      </div>
                    </Card.Body>
                  </Card>
                  <Card>
                    <Card.Body>
                      {result && (
                        <div>
                          <SignalPlot data={result.Real} title={"Real"} type={"1D"}></SignalPlot>
                          <SignalPlot data={result.Imaginary} title={"Imaginary"} type={"1D"}></SignalPlot>
                          <SignalPlot data={result.Magnitude} title={"Magnitude"} type={"1D"}></SignalPlot>
                          <SignalPlot data={result.Phase} title={"Phase"} type={"1D"}></SignalPlot>
                        </div>
                      )}
                    </Card.Body>
                  </Card>
                </Tab.Pane>
                <Tab.Pane eventKey="cosineTransform2D">
                  <Card>
                    <Card.Body>
                      <div className="form-container">
                        <Card.Title>2D Cosine Transform Calculation</Card.Title>
                        <Form onSubmit={handleSubmitCosineTransform2D}>
                          <Row className="mb-3">
                            <Col xs={5}><FormLabel>Real</FormLabel></Col>
                            <Col xs={5}><FormLabel>Imaginary</FormLabel></Col>
                          </Row>
                          {coefficients.map((coefficient, index) => (
                            <Row key={index} className="mb-3">
                              <Col xs={5}>
                                <FormControl
                                  type="text"
                                  value={coefficient.real}
                                  onChange={handleCoefficientChange(index, 'real')}
                                />
                              </Col>
                              <Col xs={5}>
                                <FormControl
                                  type="text"
                                  value={coefficient.imaginary}
                                  onChange={handleCoefficientChange(index, 'imaginary')}
                                />
                              </Col>
                              <Col>
                                {index !== 0 && (
                                  <Button variant="danger" className="ml-1" onClick={handleRemoveCoefficient(index)}>Remove</Button>
                                )}
                              </Col>
                            </Row>
                          ))}
                          <Button variant="primary" onClick={handleAddCoefficient}>Add Coefficient</Button>
                          <Button variant="success" type="submit" className="ml-2">Calculate 2D Cosine Transform</Button>
                        </Form>
                      </div>
                      <Card>
                        <Card.Body>
                          {result && (
                            <div>
                              <SignalPlot data={result.Real} title={"Real"} type={"2D"}></SignalPlot>
                            </div>
                          )}
                        </Card.Body>
                      </Card>
                    </Card.Body>
                  </Card>
                </Tab.Pane>
              </Tab.Content>
            </Tab.Container>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default CosineTransformPage;
