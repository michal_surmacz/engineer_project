import React, { useState, useEffect } from 'react';
import { Container, Col, Row, Button, Card, Nav, Form } from 'react-bootstrap';
import { Link } from 'react-scroll';
import NavigationBar from '../components/NavigationBar';
import SignalPlot from '../components/FourierPlot';

function CosineTransformPage() {
  const [signal, setSignal] = useState<number[]>([]);
  const [transformedSignal, setTransformedSignal] = useState<number[]>([]);
  const [samplingRate, setSamplingRate] = useState<number>(15);
  const [linearFactor, setLinearFactor] = useState<number>(1);
  const [evenSignal, setEvenSignal] = useState<number[]>([]);
  const [oddSignal, setOddSignal] = useState<number[]>([]);
  const [evenTransformedSignal, setEvenTransformedSignal] = useState<number[]>([]);
  const [oddTransformedSignal, setOddTransformedSignal] = useState<number[]>([]);
  const [shiftedSignal, setShiftedSignal] = useState<number[]>([]);
  const [shiftAmount, setShiftAmount] = useState<number>(0);

  const cosineTransform = (inputSignal: number[], factor: number) => {
    const N = inputSignal.length;
    const transformedSignal: number[] = [];

    for (let k = 0; k < N; k++) {
      let sum = 0;
      for (let n = 0; n < N; n++) {
        const angle = (Math.PI * (n + 0.5) * k) / N;
        sum += inputSignal[n] * factor * Math.cos(angle);
      }
      transformedSignal.push(sum);
    }

    return transformedSignal;
  };

  useEffect(() => {
    const transformed = cosineTransform(signal, linearFactor);
    setTransformedSignal(transformed);
  }, [signal, linearFactor]);

  const handleSampling = () => {
    const xValues = [];
    for (let x = 0; x <= 20; x += 1 / samplingRate) {
      xValues.push(x);
    }
    const signalArray = xValues.map(sawtoothFunction);
    setSignal(signalArray);
  };

  useEffect(() => {
    const N = 100;
    const evenSignalArray = Array.from({ length: N }, (_, i) => Math.cos(i / 10));
    const oddSignalArray = Array.from({ length: N }, (_, i) => Math.sin(i / 10));

    const evenTransformed = cosineTransform(evenSignalArray, 1);
    const oddTransformed = cosineTransform(oddSignalArray, 1);

    setEvenSignal(evenSignalArray);
    setOddSignal(oddSignalArray);
    setEvenTransformedSignal(evenTransformed);
    setOddTransformedSignal(oddTransformed);
  }, []);

  const sawtoothFunction = (x: number) => {
    return (x % (2 * Math.PI)) / Math.PI - 1;
  };

  const timeShiftFunction = (inputSignal: number[], shift: number) => {
    const shiftedSignal: number[] = [];
    const N = inputSignal.length;
    
    for (let i = 0; i < N; i++) {
      const shiftedIndex = (i + shift) % N;
      shiftedSignal[shiftedIndex] = inputSignal[i];
    }

    return shiftedSignal;
  };

  const handleShift = () => {
    const shift = shiftAmount % signal.length;
    const shifted = timeShiftFunction(signal, shift);
    const shiftedTransformed = cosineTransform(shifted, linearFactor);
    setShiftedSignal(shifted);
    setTransformedSignal(shiftedTransformed);
  };

  return (
    <div>
      <NavigationBar />
      <Container fluid>
        <Row>
          <Col sm={3}>
            <NavigationMenu />
          </Col>
          <Col sm={9}>
            <Card id="introduction" className="transform-card">
              <Card.Body>
                <Card.Title>Introduction</Card.Title>
                <Card.Text>
                  The Cosine Transform is a mathematical operation used in signal processing to analyze and synthesize signals similar to the Fourier Transform. It transforms a sequence of real-valued numbers representing a discrete signal from its time domain representation into its frequency domain representation.
                </Card.Text>
              </Card.Body>
            </Card>
            <Card id="math-transform" className="transform-card">
              <Card.Body>
                <Card.Title>Math about Transform</Card.Title>
                <Card.Text>
                  The Cosine Transform is a mathematical operation used in signal processing to analyze and synthesize signals similar to the Fourier Transform. It represents a signal in terms of its frequency components rather than its time-domain representation.
                </Card.Text>
                <Card.Text>
                  The Cosine Transform of a function \( f(x) \) is defined as:
                </Card.Text>
                <Card.Text>
                  \[ F(k) = \int_-\infty^\infty f(x) \cos(kx) \, dx \]
                </Card.Text>
                <Card.Text>
                  where \( k \) is the frequency and \( F(k) \) is the corresponding frequency-domain representation of the signal.
                </Card.Text>
                <Card.Text>
                  In the discrete domain, for a discrete signal \( x[n] \), the Cosine Transform is given by:
                </Card.Text>
                <Card.Text>
                  where \( N \) is the number of samples in the signal and \( X(k) \) is the transformed signal.
                </Card.Text>
                <Card.Text>
                  The Cosine Transform provides valuable insights into the frequency components of a signal, allowing for various signal processing tasks such as
                </Card.Text>
              </Card.Body>
            </Card>
            <Card id="properties" className="transform-card">
              <Card.Body>
                <Card.Title>Transform Properties Section</Card.Title>
                <Card.Text>
                  This section discusses various properties of the transform such as linearity, symmetry, etc.
                </Card.Text>
              </Card.Body>
            </Card>
            <Card id="linearity" className="transform-card">
              <Card.Body>
                <Card.Title>Linearity</Card.Title>
                <Card.Text>
                  The cosine transform is linear, which means that scaling the input signal results in a corresponding scaling of the transformed signal. Let's explore this property further.
                </Card.Text>
                <Card.Text>
                  Consider two signals, f(x) and g(x), with corresponding cosine transforms F(k) and G(k). The linearity property states:
                </Card.Text>

                <Card.Text>
                  where a and b are constants.
                </Card.Text>
                <Card.Text>
                  To demonstrate linearity, we'll sample two different signals, scale them using different factors, and observe how the transformed signals scale accordingly.
                </Card.Text>
                <Button variant="primary" onClick={handleSampling}>
                  Sample Signals
                </Button>
              </Card.Body>
              <Card.Body>
                <Card.Title>Saw Function and Transformed Signal</Card.Title>
                <Row>
                  <Col>
                    {signal.length > 0 && <SignalPlot data={signal} title="Saw Function" type="1D" />}
                  </Col>
                  <Col>
                    {transformedSignal.length > 0 && <SignalPlot data={transformedSignal} title={`Transformed Signal (Linear Factor: ${linearFactor})`} type="1D" />}
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Group as={Row}>
                      <Form.Label column sm="4">Linear Factor:</Form.Label>
                      <Col sm="8">
                        <input type="range" min="1" max="10" value={linearFactor} onChange={(e) => setLinearFactor(Number(e.target.value))} />
                      </Col>
                    </Form.Group>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
            <Card id="symmetry" className="transform-card">
              <Card.Body>
                <Card.Title>Symmetry</Card.Title>
                <Card.Text>
                  The cosine transform exhibits symmetry properties, specifically even and odd symmetry.
                </Card.Text>
                <Card.Text>
                  A signal is even symmetric if \( f(x) = f(-x) \) for all \( x \). In the frequency domain, even symmetry implies that \( F(k) = F(-k) \) for all \( k \).
                </Card.Text>
                <Card.Text>
                  A signal is odd symmetric if \( f(x) = -f(-x) \) for all \( x \). In the frequency domain, odd symmetry implies that \( F(k) = -F(-k) \) for all \( k \).
                </Card.Text>
                <Card.Text>
                  Let's demonstrate these symmetry properties by sampling even and odd symmetric signals and observing their transformed signals.
                </Card.Text>
                <Row>
                  <Col>
                    {evenSignal.length > 0 && <SignalPlot data={evenSignal} title="Even Symmetric Signal" type="1D" />}
                    {evenTransformedSignal.length > 0 && <SignalPlot data={evenTransformedSignal} title="Transformed Even Symmetric Signal" type="1D" />}
                  </Col>
                  <Col>
                    {oddSignal.length > 0 && <SignalPlot data={oddSignal} title="Odd Symmetric Signal" type="1D" />}
                    {oddTransformedSignal.length > 0 && <SignalPlot data={oddTransformedSignal} title="Transformed Odd Symmetric Signal" type="1D" />}
                  </Col>
                </Row>
              </Card.Body>
            </Card>
            <Card id="time-shifting" className="transform-card">
              <Card.Body>
                <Card.Title>Time Shifting</Card.Title>
                <Row>
                  <Col>
                    <Form.Group as={Row}>
                      <Form.Label column sm="4">Shift Amount:</Form.Label>
                      <Col sm="8">
                        <input type="number" value={shiftAmount} onChange={(e) => setShiftAmount(Number(e.target.value))} />
                      </Col>
                    </Form.Group>
                    <Button variant="primary" onClick={handleShift}>
                      Apply Shift
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {shiftedSignal.length > 0 && <SignalPlot data={shiftedSignal} title={`Shifted Signal (Shift Amount: ${shiftAmount})`} type="1D" />}
                  </Col>
                  <Col>
                    {transformedSignal.length > 0 && <SignalPlot data={transformedSignal} title={`Transformed Signal (Shift Amount: ${shiftAmount})`} type="1D" />}
                  </Col>
                </Row>
              </Card.Body>
            </Card>
            <Card id="plot" className="transform-card">
              
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

const NavigationMenu = () => {
  const sections = ['Introduction', 'Math about Transform', 'Transform Properties Section', 'Linearity', 'Saw Function and Transformed Signal', 'Symmetry', 'Time Shifting'];

  return (
    <Card className="sticky-top">
      <Card.Body>
        <Card.Title>Transform</Card.Title>
        <Nav className="flex-column">
          {sections.map((section, index) => (
            <Nav.Link key={index} as={Link} to={section.toLowerCase().replace(/\s/g, '-')} smooth={true} duration={500}>{section}</Nav.Link>
          ))}
        </Nav>
      </Card.Body>
    </Card>
  );
};

export default CosineTransformPage;
