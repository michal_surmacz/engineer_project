import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import './styles.css';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import NavigationBar from '../components/NavigationBar';

const WelcomePage: React.FC = () => {
  const [showWelcome, setShowWelcome] = useState(false);
  const history = useHistory();

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowWelcome(true);
    }, 500);

    return () => clearTimeout(timer);
  }, []);

  const handleTransformSelection = (transformType: string, option: string) => {
    if (transformType === 'fourier') {
      history.push(`/fourier/${option}`);
    } else if (transformType === 'cosine') {
      history.push(`/cosine/${option}`);
    }
  };

  return (
    <div>
    <NavigationBar/>
      <Container>
        <Row className="justify-content-center">
          <Col md={6}>
            <Card className="transform-card">
              <Card.Body>
                <Card.Title>Fourier Transform</Card.Title>
                <Card.Text>
                  Dive deep into the world of Fourier Transforms, understand its principles, and see how it’s used in real-world applications.
                </Card.Text>
                <Button variant="primary" onClick={() => handleTransformSelection('fourier', 'calculator')}>Calculator</Button>
                <Button variant="primary" onClick={() => handleTransformSelection('fourier', 'learning')}>Learning</Button>
              </Card.Body>
            </Card>
          </Col>
          <Col md={6}>
            <Card className="transform-card">
              <Card.Body>
                <Card.Title>Cosine Transform</Card.Title>
                <Card.Text>
                  Explore the fascinating Cosine Transform, its mathematical foundation, and its practical uses in various fields.
                </Card.Text>
                <Button variant="primary" onClick={() => handleTransformSelection('cosine', 'calculator')}>Calculator</Button>
                <Button variant="primary" onClick={() => handleTransformSelection('cosine', 'learning')}>Learning</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default WelcomePage;
