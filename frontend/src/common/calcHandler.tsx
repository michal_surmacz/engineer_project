export const fetchData = async (
    url: string,
    method: string,
    headers: Record<string, string>,
    body: string
  ): Promise<any> => {
    try {
      const response = await fetch(url, {
        method,
        headers,
        body,
      });
  
      if (response.ok) {
        return await response.json();
      } else {
        throw new Error('Failed to perform calculation');
      }
    } catch (error) {
      throw new Error(`Error: ${error}`);
    }
  };
  