export const DFTCALCULATIONS = () => "http://localhost:5000/api/dft/calculation"
export const IDFTCALCULATIONS = () => "http://localhost:5000/api/idft/calculation"
export const DCTCALCULATIONS = () => "http://localhost:5000/api/dct/calculation"
export const DFT2DCALCULATIONS = () => "http://localhost:5000/api/dft2d/calculation"
export const DCT2DCALCULATIONS = () => "http://localhost:5000/api/dct2d/calculation"

export const fetchData = async (url: string, method: string, headers: any, body: string) => {
    try {
      const response = await fetch(url, {
        method,
        headers,
        body,
      });
  
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
  
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching data:', error);
      throw error;
    }
  };