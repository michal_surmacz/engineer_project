from ..celery_app import celery
from ..calculators.dft_calculator import DftCalculator
from ..calculators.dct_calculator import DctCalculator
from ..cache.managers import DftCacheMenager, QueueManager, DctCacheMenager

@celery.task(name="calculate_dft")
def calculate_dft(values_list):
    result = DftCalculator().calculate_dft(values_list)
    dft_queue = DftCacheMenager(QueueManager().cache)
    dft_queue.set_expire("dft", result, 2)
    return result


@celery.task(name="calculate_idft")
def calculate_idft(values_list):
    result = DftCalculator().calculate_idft(values_list)
    dft_queue = DftCacheMenager(QueueManager().cache)
    dft_queue.set_expire("idft", result, 2)
    return result

@celery.task(name="calculate_dft2d")
def calculate_dft2d(values_list):
    print(values_list)
    result = DftCalculator().calculate_dft2d(values_list)
    dft_queue = DftCacheMenager(QueueManager().cache)
    dft_queue.set_expire("dft2d", result, 2)
    return result


@celery.task(name="calculate_idft")
def calculate_idft2d(values_list):
    result = DftCalculator().calculate_idft2d(values_list)
    dft_queue = DftCacheMenager(QueueManager().cache)
    dft_queue.set_expire("idft2d", result, 2)
    return result

@celery.task(name="calculate_dct")
def calculate_dct(values_list):
    result = DctCalculator().calculate_dct(values_list)
    dft_queue = DctCacheMenager(QueueManager().cache)
    dft_queue.set_expire("dct", result, 2)
    return result


@celery.task(name="calculate_idct")
def calculate_idct(values_list):
    result = DctCalculator().calculate_idct(values_list)
    dft_queue = DctCacheMenager(QueueManager().cache)
    dft_queue.set_expire("idct", result, 2)
    return result

@celery.task(name="calculate_dct2d")
def calculate_dct2d(values_list):
    result = DctCalculator().calculate_dct2d(values_list)
    dft_queue = DctCacheMenager(QueueManager().cache)
    dft_queue.set_expire("dct2d", result, 2)
    return result


@celery.task(name="calculate_idct")
def calculate_idct2d(values_list):
    result = DctCalculator().calculate_idct2d(values_list)
    dft_queue = DctCacheMenager(QueueManager().cache)
    dft_queue.set_expire("idct2d", result, 2)
    return result