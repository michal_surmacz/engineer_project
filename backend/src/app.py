from __future__ import annotations

from flask import Flask
from flask_cors import CORS
from .common.utils import get_config
from flask_restful import Api as FlaskRestApi
from .routers._base import initialize_routes
from .cache.managers import CacheManager

class App:
    def __init__(self):
        self.cfg = get_config()
        self.app = Flask(__name__)
        self.api = FlaskRestApi(self.app, errors={})
        initialize_routes(self.api)
        self.cors, self.cors_origins = self._build_cors()
        self.cache = CacheManager.create(self.cfg.CACHING)

    def _build_cors(self) -> tuple[CORS | None, list]:
        origins = ["http:///localhost:5000", "http://localhost:8000", "http:///localhost:3000"]
        if self.cfg.CORS:
            cors = CORS(
                self.app,
                supports_credentials=True,
                origins="*",
            )
            print("CORS ACTIVE")
        else:
            cors = None
        return cors, origins

if __name__ == '__main__':
    app = App()
    app.app.run(debug=True)