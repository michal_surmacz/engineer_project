from .cache.interface import RedisCache

class Config():
    CORS = True
    REDIS_URL="redis://redis:6379"
    WORKER_CONCURRENCY = 2
    CACHING = RedisCache("redis://localhost:6379")

class DevelopConfig():
    CORS = False
    REDIS_URL="redis://localhost:6379"
    WORKER_CONCURRENCY = 2
    CACHING = RedisCache("redis://localhost:6379")


class ProductionConfig():
    CORS = True
    REDIS_URL="redis://localhost:6379"
    WORKER_CONCURRENCY = 32
    CACHING = RedisCache("redis://127.0.0.1:6379")