from flask_restful import Api
from ..routers.calculation import *

def initialize_routes(api: Api):
    resource_route= {
        DftCalculationApi: "/api/dft/calculation",
        IdftCalculationApi: "/api/idft/calculation",
        Dft2dCalculationApi: "/api/dft2d/calculation",
        Idft2dCalculationApi: "/api/idft2d/calculation",
        DctCalculationApi: "/api/dct/calculation",
        IdctCalculationApi: "/api/idct/calculation",
        Dct2dCalculationApi: "/api/dct2d/calculation",
        Idct2dCalculationApi: "/api/idct2d/calculation",
    }
    for resource, route in resource_route.items():
        api.add_resource(resource, route)