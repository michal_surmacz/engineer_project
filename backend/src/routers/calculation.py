from flask_restful import Resource
from flask import request
from werkzeug.utils import secure_filename
import numpy as np
from PIL import Image
from ..workers.calc_worker import *

class DftCalculationApi(Resource):
    def post(self):
        request_data = request.get_json()
        result = calculate_dft.apply_async(args=[request_data], task_id="dft")
        dft_result = result.get(timeout=None)
        result.forget()
        return dft_result, 200

class IdftCalculationApi(Resource):
    def post(self):
        request_data = request.get_json()
        result = calculate_idft.apply_async(args=[request_data], task_id="idft")
        idft_result = result.get(timeout=None)
        result.forget()
        return idft_result, 200
    
class Dft2dCalculationApi(Resource):
    def post(self):
        if 'image' not in request.files:
            return {"error": "No image file provided"}, 400
        
        image_file = request.files['image']
        
        if image_file.filename == '':
            return {"error": "No selected file"}, 400
        
        try:
            image = Image.open(image_file)
            image = image.convert('L')
            image = image.resize((128, 128))
            pixel_data = np.array(image)
            print(pixel_data)
            
            result = calculate_dft2d.apply_async(args=[pixel_data.tolist()], task_id="dft2d")
            dft2d_result = result.get(timeout=None)
            result.forget()
            
            return dft2d_result, 200
        except Exception as e:
            return {"error": str(e)}, 500

class Idft2dCalculationApi(Resource):
    def post(self):
        request_data = request.get_json()
        result = calculate_idft2d.apply_async(args=[request_data], task_id="idft2d")
        idft2d_result = result.get(timeout=None)
        result.forget()
        return idft2d_result, 200

class DctCalculationApi(Resource):
    def post(self):
        request_data = request.get_json()
        result = calculate_dct.apply_async(args=[request_data], task_id="dct")
        dct_result = result.get(timeout=None)
        result.forget()
        return dct_result, 200

class IdctCalculationApi(Resource):
    def post(self):
        request_data = request.get_json()
        result = calculate_idct.apply_async(args=[request_data], task_id="idct")
        idct_result = result.get(timeout=None)
        result.forget()
        return idct_result, 200
    
class Dct2dCalculationApi(Resource):
    def post(self):
        request_data = request.get_json()
        result = calculate_dct2d.apply_async(args=[request_data], task_id="dct2d")
        dct2d_result = result.get(timeout=None)
        result.forget()
        return dct2d_result, 200

class Idct2dCalculationApi(Resource):
    def post(self):
        request_data = request.get_json()
        result = calculate_idct2d.apply_async(args=[request_data], task_id="idct2d")
        idct2d_result = result.get(timeout=None)
        result.forget()
        return idct2d_result, 200
