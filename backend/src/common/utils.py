import os
from ..common.errors import NoTraceError

def get_config():
    if os.getenv("APP_MODE") == "PRODUCTION":
        from src.config import Config
        cfg = Config
    elif os.getenv("APP_MODE") == "DEVELOPMENT":
        from src.config import DevelopConfig
        cfg = DevelopConfig
    else: 
        raise NoTraceError("You need to set APP_MODE env to one of PRODUCTION/DEVELOPMENT")
    return cfg