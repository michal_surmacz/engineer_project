import sys

class NoTraceError(Exception):
    def __init__(self, *args: object) -> None:
        sys.tracebacklimit = 0
    
    def __del__(self):
        sys.tracebacklimit = 100

class CacheMiss(Exception):
    pass