from celery import Celery
from .common.utils import get_config
from .cache.managers import QueueManager
from .cache.interface import RedisCache


cfg = get_config()

celery = Celery(__name__)
celery.conf.broker_url = cfg.REDIS_URL
celery.conf.result_backend = cfg.REDIS_URL
celery.conf.update(worker_concurrency=cfg.WORKER_CONCURRENCY)

cache_menager = QueueManager.create(RedisCache(cfg.REDIS_URL))

celery.autodiscover_tasks(["src.workers.calc_worker"], force = True)