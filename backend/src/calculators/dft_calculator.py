import numpy as np

class DftCalculator:
    def __init__(self) -> None:
        pass

    def calculate_dft(self, values):
        try:
            values_list = [complex(float(val['real'])) for val in values['coefficients']]
        except (KeyError, ValueError, TypeError) as e:
            print(e)
            return None

        try:
            result = np.fft.fft(values_list)
            result_dict = {'Magnitude': [np.abs(x) for x in result]}
            return result_dict
        except Exception as e:
            print(f"Error during DFT calculation: {e}")
            return None  
        
    def calculate_idft(self, values):
        try:
            values_list = [complex(float(val['real']), float(val['imaginary'])) for val in values['coefficients']]
        except (KeyError, ValueError, TypeError) as e:
            print(e)
            return None

        try:
            result = np.fft.ifft(values_list)
            result_dict = {'Real': [np.real(x) for x in result]}
            return result_dict
        except Exception as e:
            print(f"Error during IDFT calculation: {e}")
            return None  
        
    def calculate_dft2d(self, values):
        try:
            result = np.fft.fft2(values)
            ftimage = np.fft.fftshift(result)
            result_dict = {'Magnitude': np.abs(ftimage).tolist()}
            return result_dict
        except Exception as e:
            print(f"Error during DFT2D calculation: {e}")
            return None 
        
    def calculate_idft2d(self, values):
        try:
            values_list = np.array([[complex(float(val['real']), float(val['imaginary'])) for val in row] for row in values['coefficients']])
        except (KeyError, ValueError, TypeError) as e:
            print(e)
            return None

        try:
            result = np.fft.ifft2(values_list)
            result_dict = {'Real': np.real(result).tolist()}
            return result_dict
        except Exception as e:
            print(f"Error during IDFT2D calculation: {e}")
            return None
