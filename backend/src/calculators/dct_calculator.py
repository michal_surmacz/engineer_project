import numpy as np
import scipy

class DctCalculator:
    def __init__(self) -> None:
        pass

    def calculate_dct(self, values):
        print(values)
        try:
            values_list = [float(val['real']) for val in values['coefficients']]
        except (KeyError, ValueError, TypeError) as e:
            print(e)
            return None

        try:
            result = scipy.fft.dct(values_list, norm='ortho')
            result_dict = {'Real': result.tolist()}
            return result_dict
        except Exception as e:
            print(f"Error during DCT calculation: {e}")
            return None  
    
    def calculate_idct(self, values):
        print(values)
        try:
            values_list = [float(val['real']) for val in values['coefficients']]
        except (KeyError, ValueError, TypeError) as e:
            print(e)
            return None

        try:
            result = scipy.fft.idctn(values_list, norm='ortho')
            result_dict = {'Real': result.tolist()}
            return result_dict
        except Exception as e:
            print(f"Error during IDCT calculation: {e}")
            return None  
        
    def calculate_dct2d(self, values):
        try:
            values_list = [[float(val['real']) for val in row] for row in values['coefficients']]
        except (KeyError, ValueError, TypeError) as e:
            print(e)
            return None

        try:
            result = scipy.fft.dct2d(values_list, type=2, norm='ortho')
            result_dict = {'Real': result.tolist()}
            return result_dict
        except Exception as e:
            print(f"Error during DCT2D calculation: {e}")
            return None 
        
    def calculate_idct2d(self, values):
        try:
            values_list = [[float(val['real']) for val in row] for row in values['coefficients']]
        except (KeyError, ValueError, TypeError) as e:
            print(e)
            return None
        try:
            result = np.fft.idctn(values_list, type=2, norm='ortho')
            result_dict = {'Real': result.tolist()}
            return result_dict
        except Exception as e:
            print(f"Error during IDCT2D calculation: {e}")
            return None
