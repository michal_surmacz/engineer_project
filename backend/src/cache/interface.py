from abc import ABC, abstractmethod
import redis
import pickle
from ..common.errors import CacheMiss

class Cache(ABC):
    @abstractmethod
    def set(self, key: str, val):
        pass
    @abstractmethod
    def keys(self, pattern: str):                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        pass
    @abstractmethod
    def get(self, key: str):
        pass
    @abstractmethod
    def delete(self, key: str):
        pass
    @abstractmethod
    def set_expire(self, key: str, val, expire: float):
        pass

class RedisCache(Cache):
    def __init__(self, url="redis://localhost:6379/0") -> None:
        self.r = redis.from_url(url)

    def set(self, key: str, val):
        self.r.set(key, pickle.dumps(val))
    
    def keys(self, pattern: str):
        return self.r.keys(pattern)

    def get(self, key: str):
        cache = self.r.get(key)
        if not cache:
            raise CacheMiss
        unpacked = pickle.loads(cache)
        if not unpacked:
            raise CacheMiss
        return unpacked

    def set_expire(self, key: str, val, expire: float):
        self.r.set(key, pickle.dumps(val), expire)

    def delete(self, key: str):
        print(key)
        cache = self.get(key)
        self.r.delete(key)
        return cache