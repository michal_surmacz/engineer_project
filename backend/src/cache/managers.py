from ..common.singleton_metaclass import SingletonMeta
from .interface import Cache

class CacheManager(metaclass=SingletonMeta):
    _cache = None

    def __init__(self):
        ...
    
    @classmethod
    def create(cls, cache = Cache):
        cls()._cache = cache

    @property
    def cache(self):
        if self._cache is None:
            raise Exception("Cache uninitialized")
        return self._cache

class QueueManager(CacheManager):
    _cache= None

class DftCacheMenager:
    def __init__(self, cache: Cache) -> None:
        self.scoped = ScopedCacheMenager("dft", cache)
    
    def get(self, key):
        return self.scoped.get(key)
    
    def delete(self, key):
        return self.scoped.delete(key)
    
    def flush(self):
        self.scoped._cache.r.flushdb()

    def set_expire(self, key, val, expire: float):
        self.scoped.set_expire(key, val, expire)



class DctCacheMenager:
    def __init__(self, cache: Cache) -> None:
        self.scoped = ScopedCacheMenager("dct", cache)
    
    def get(self, key):
        return self.scoped.get(key)
    
    def delete(self, key):
        return self.scoped.delete(key)
    
    def flush(self):
        self.scoped._cache.r.flushdb()

    def set_expire(self, key, val, expire: float):
        self.scoped.set_expire(key, val, expire)



class ScopedCacheMenager:
    def __init__(self, scope_key: str, cache = Cache) -> None:
        self._scope = scope_key
        self._cache = cache
        print(scope_key)
        print(self._cache)
    
    def __translate(self, key):
        return f"{self._scope}: {key}"
    
    def set(self, key: str, val):
        return self._cache.set(self.__translate(key), val)
    
    def get(self, key: str):
        return self._cache.get(self.__translate(key))
    
    def delete(self, key: str):
        print(self.__translate(key))
        return self._cache.delete(self.__translate(key))
    
    def set_expire(self, key, val, expire):
        return self._cache.set_expire(self.__translate(key),val, expire)