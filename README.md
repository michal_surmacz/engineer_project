# Jak uruchomić aplikację?

Aplikacja posiada zaimplementowany system zarządzania kontenerami - Docker Compose.
Użyte kontenery:
 - Frontend - odpowiedzialny jest za wyświetlanie aplikacji na stronie
 - Backend - przetwarzanie zapytań z frontendu oraz delegowanie obliczeń
 - Worker - zadania do policzenia przez kalkulator delegowane są do workerów, które jako działają osobne procesy 


Do uruchomienia aplikacji w środowisku Linux wystarczy zainstalować pakiet docker-compose (najlepiej w wersji 1.25.0) za pomocą komendy:
- sudo apt install docker-compose
Aby sprawdzić poprawną instalację można użyć komendy "docker-compose -v".

Gdy pakiet docker-compose jest zainstalowany poprawnie całą aplikację można uruchomić za pomocą komendy:
- "docker-compose up --build -d"
Kontenery zbudują się a następnie uruchomią. Strona będzie widoczna na wewnętrznym adresie localhost na porcie 3000. 